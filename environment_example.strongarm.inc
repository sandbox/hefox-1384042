<?php
/**
 * @file
 * tt_event_vitals_ftm.strongarm.inc
 */

/**
 * Implementation of hook_strongarm().
 */
function environment_example_strongarm() {
  $export = array();
  $development = environment_current() == 'development';

  $variables = array(
    'error_level' => $development ? 2 : 0,
    'cache' => $development ? FALSE : TRUE,
    'preprocess_css' => !$development,
    'preprocess_js' => !$development,
  );

  foreach ($variables as $name => $value) {
    $strongarm = new stdClass;
    $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
    $strongarm->api_version = 1;
    $strongarm->name = $name;
    $strongarm->value = $value;
    $export[$name] = $strongarm;
  }

  return $export;
}
